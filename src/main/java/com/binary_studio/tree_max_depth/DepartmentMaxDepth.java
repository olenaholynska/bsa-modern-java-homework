package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) return 0;

		int height = 0;

		// level representing queue
		Queue<Node> nodesQueue = new LinkedList<>();
		nodesQueue.add(new Node(rootDepartment));

		while (true) {
			// amount of nodes on level
			int nodesCount = nodesQueue.size();
			if (nodesCount == 0) return height;

			++height;

			// deque all nodes of current level and
			// enqueue all nodes of next level
			while (nodesCount > 0) {
				var currentLevelNode = nodesQueue.remove();
				var validChildrenNodes = currentLevelNode.children
						.parallelStream()
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
				nodesQueue.addAll(validChildrenNodes);
				--nodesCount;
			}
		}
	}

}
