package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public final class Department {

	public final String name;

	public final List<Department> subDepartments;

	public Department(String name) {
		this.name = name;
		this.subDepartments = new ArrayList<>();
	}

	public Department(String name, Department... departments) {
		this.name = name;
		this.subDepartments = Arrays.asList(departments);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Department that = (Department) o;
		return Objects.equals(name, that.name) &&
				Objects.equals(subDepartments, that.subDepartments);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, subDepartments);
	}

}
