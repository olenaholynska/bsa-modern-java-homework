package com.binary_studio.tree_max_depth;
import java.util.List;

class Node {
    Department data;
    List<Node> children;

    public Node (Department data, List<Node> children) {
        this.data = data;
        this.children = children;
    }
    public Node (Department data) {
        this.data = data;
        this.children = null;
    }
}